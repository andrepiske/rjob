[![CI](https://gitlab.com/andrepiske/rjob/badges/master/pipeline.svg)](https://gitlab.com/andrepiske/rjob/-/jobs)
[![Gem Version](https://badge.fury.io/rb/rjob.svg)](https://badge.fury.io/rb/rjob)

# rjob

An async job processor. Similar to what Resque and Sidekiq do,
but with different design goals.

# Testing

Install [Earthly](https://earthly.dev/), then run:

```shell
$ earthly +test
```

# License

See the LICENSE file.

Copyright 2021-2022 André Diego Piske
