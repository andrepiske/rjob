# frozen_string_literal: true

module Rjob
end

class Rjob::CLI
  def self.boot; new.boot(ARGV); end

  def initialize
    @use_rails = false
    @run_workers = false
    @requires = []
  end

  def boot(args)
    STDOUT.sync = true
    STDERR.sync = true

    parse_cli_args(args)

    if @use_rails
      require File.join(Dir.pwd, "config/environment")
    end

    @requires.each do |r|
      require r
    end

    run_workers if @run_workers
  end

  def run_workers
    require "rjob"
    require "rjob/worker_process"

    worker = Rjob::WorkerProcess.new(Rjob::Context.instance)
    worker.run_forever
  end

  private

  def parse_cli_args(args)
    while args.length > 0 do
      arg = args.shift
      if arg == "--use-rails"
        @use_rails = true
      elsif arg == "--run-workers"
        @run_workers = true
      elsif arg =~ /^--require=/
        @requires << arg[10..-1]
      else
        puts "Unrecognized argument: #{arg}"
        puts "Exiting now"
        exit 1
      end
    end
  end
end
