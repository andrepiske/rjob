# frozen_string_literal: true

# Processes one single job.
class Rjob::JobProcessor
  attr_reader :context
  attr_reader :error
  attr_reader :job_str
  attr_reader :job

  def initialize(context, job_str)
    @context = context
    @job_str = job_str
    @error = nil
    @force_dont_retry = false
    @success = false
  end

  def success?
    @success
  end

  def stop_retry?
    @force_dont_retry
  end

  def run
    job = Rjob::Job.deserialize(@context, @job_str)
    @job = job

    job_args = job.worker_args

    worker_class = begin
      job.worker_class
    rescue NameError
      @error = { message: "No worker class '#{job.worker_class_name}'" }
      @force_dont_retry = true
      return
    end

    begin
      worker_instance = worker_class.new(@context, job)
      worker_instance.perform(*job_args)
      @success = true
    rescue Exception => e
      @error = { error_class: e.class, message: e.message }
    end
  end
end

#     @thread_pool.post do
#       begin
#         klass, args = MessagePack.unpack(job_data)
#         ::Rjob::WorkerThread.new(self, klass, args).run
#       rescue Exception => e
#         @failed_count.increment
#         handle_failed_job(job, bucket, e, klass, args)
#       ensure
#         @processed_count.increment
#       end
#     end
#   rescue Rjob::Job::DeserializationError => e
#     @error = { exception: e }
#   end
# end

#
# class Rjob::WorkerThread
#   def initialize(worker, klass, args)
#     @worker = worker
#     @context = @worker.context
#     @prefix = @context.prefix
#     @job_class, @job_args = klass, args
#   end
#
#   def run
#     klass = @context.demodularize_class(@job_class)
#     klass.perform(@job_args)
#   end
# end
#
