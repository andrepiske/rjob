# frozen_string_literal: true

# This file just loads dependencies needed for running
# recurring jobs

require 'openssl'
require 'time'
require 'date'

begin
  require 'fugit'
rescue LoadError => e
  puts("The gem 'fugit' is required when recurring_jobs config is set for Rjob")
  raise(e)
end

require 'rjob'
require 'rjob/recurring_job'
