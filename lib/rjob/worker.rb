# frozen_string_literal: true

class Rjob::Worker
  attr_reader :context
  attr_reader :job

  def initialize(context, job)
    @context = context
    @job = job
  end

  def self.retry_options
    {
      retry: false
    }
  end
end
