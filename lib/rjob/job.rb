# frozen_string_literal: true

class Rjob::Job
  DeserializationError = Class.new(StandardError)

  attr_accessor :id
  attr_accessor :retry_num
  attr_reader :payload
  attr_reader :context

  def initialize(context)
    @context = context
  end

  def worker_class_name
    @deserialized_payload[0]
  end

  def worker_class
    @context.fetch_worker_class(class_name: worker_class_name)
  end

  def worker_args
    @deserialized_payload[1]
  end

  def payload=(str)
    @payload = str
    @deserialized_payload = MessagePack.unpack(str)
  end

  def serialize
    "#{@id}!#{@retry_num}!#{@payload}".b
  end

  def self.deserialize(context, job_str)
    first = job_str.index('!')
    second = job_str.index('!', first + 1)

    if first == nil || second == nil
      raise DeserializationError.new("Malformed job string: '#{job_str}'")
    end

    begin
      new(context).tap do |job|
        job.id = job_str[0...first]
        job.retry_num = job_str[(first + 1)...second].to_i
        job.payload = job_str[(second + 1)..-1]
      end
    rescue MessagePack::MalformedFormatError => e
      raise DeserializationError.new("Malformed job msgpack payload: #{e.message}")
    end
  end
end
