# frozen_string_literal: true

class Rjob::Scripts::EnqueueScheduledJobs < Rjob::Scripts::RedisScript
  def arg_params
    %i(time_now job_limit bucket_no)
  end

  def key_params
    %i(scheduled_key dest_key jobs_key)
  end

  def lua_script
    <<~LUA
      local r = redis
      local time_now = ARGV[1]
      local job_limit = ARGV[2]
      local bucket_no = ARGV[3]

      local scheduled_key = KEYS[1]
      local dest_key = KEYS[2]
      local jobs_key = KEYS[3]

      local jobs = r.call('zrangebyscore', scheduled_key, 0, time_now, 'limit', 0, job_limit)
      if #jobs == 0 then
        return 0
      end

      local i
      for i=1, #jobs do
        r.call('lpush', dest_key, jobs[i])
      end
      r.call('zrem', scheduled_key, unpack(jobs))
      r.call('publish', jobs_key, bucket_no)

      return #jobs
    LUA
  end
end
