# frozen_string_literal: true

class Rjob::Scripts::ScanBuckets < Rjob::Scripts::RedisScript
  def arg_params
    %i(prefix bucket_count)
  end

  def lua_script
    <<~LUA
      local prefix = ARGV[1]
      local bucket_count = ARGV[2]
      local r = redis
      local i
      for i=0,bucket_count-1 do
        local len = r.call('llen', prefix .. ':jobs:' .. i)
        if len > 0 then
          r.call('publish', prefix .. ':jobs', tostring(i))
        end
      end
      return 1
    LUA
  end
end
