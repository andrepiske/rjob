# frozen_string_literal: true

class Rjob::Scripts::ScheduleJobAt < Rjob::Scripts::RedisScript
  def arg_params
    %i(timestamp job prefix bucket_count)
  end

  def lua_script
    <<~LUA
      local timestamp = ARGV[1]
      local job = ARGV[2]
      local prefix = ARGV[3]
      local bucket_count = tonumber(ARGV[4])
      local r = redis
      local job_id = r.call('incr', prefix .. ':next')
      local bucket = job_id % bucket_count
      r.call('zadd', prefix .. ':scheduled:' .. bucket, timestamp, job_id .. '!0!' .. job)
      return job_id
    LUA
  end
end
