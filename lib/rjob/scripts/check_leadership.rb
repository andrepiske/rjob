# frozen_string_literal: true

class Rjob::Scripts::CheckLeadership < Rjob::Scripts::RedisScript
  def arg_params
    %i(worker_name time_now prefix heartbeat_timeout)
  end

  def lua_script
    <<~LUA
      local worker_name = ARGV[1]
      local time_now = ARGV[2]
      local prefix = ARGV[3]
      local heartbeat_timeout = tonumber(ARGV[4])
      local r = redis
      if r.call('setnx', prefix .. ':leaderworker', worker_name) == 1 then
        return worker_name
      else
        local leader = r.call('get', prefix .. ':leaderworker')
        local last_hb = tonumber(r.call('hget', prefix .. ':worker:' .. leader, 'heartbeat'))
        if last_hb == nil or time_now - last_hb > heartbeat_timeout then
          r.call('set', prefix .. ':leaderworker', worker_name)
          return worker_name
        end
        return leader
      end
    LUA
  end
end
