# frozen_string_literal: true

class Rjob::Scripts::RetryJob < Rjob::Scripts::RedisScript
  def arg_params
    %i(next_retry_at retry_num bucket job_id job_payload prefix)
  end

  def lua_script
    <<~LUA
      local timestamp = ARGV[1]
      local retry_num = ARGV[2]
      local bucket = ARGV[3]
      local job_id = ARGV[4]
      local job_payload = ARGV[5]
      local prefix = ARGV[6]
      local r = redis

      local curr_job = job_id .. '!' .. retry_num .. '!' .. job_payload
      local new_job = job_id .. '!' .. (retry_num + 1) .. '!' .. job_payload

      r.call('lrem', prefix .. ':jobs:' .. bucket .. ':working', 1, curr_job)
      r.call('zadd', prefix .. ':scheduled:' .. bucket, timestamp, new_job)

      return job_id
    LUA
  end
end
