# frozen_string_literal: true

class Rjob::Scripts::EnqueueJob < Rjob::Scripts::RedisScript
  def arg_params
    %i(prefix bucket_count job_data)
  end

  def lua_script
    <<~LUA
      local prefix = ARGV[1]
      local bucket_count = tonumber(ARGV[2])
      local job_data = ARGV[3]
      local r = redis
      local job_id = r.call('incr', prefix .. ':next')
      local bucket = job_id % bucket_count
      r.call('lpush', prefix .. ':jobs:' .. bucket, job_id .. '!0!' .. job_data)
      r.call('publish', prefix .. ':jobs', tostring(bucket))
      return job_id
    LUA
  end
end
