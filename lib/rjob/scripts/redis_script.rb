# frozen_string_literal: true

class Rjob::Scripts::RedisScript
  attr_accessor :sha1

  def arg_params
    []
  end

  def key_params
    []
  end
end
