# frozen_string_literal: true

class Rjob::Scripts::ReturnJobExecution < Rjob::Scripts::RedisScript
  def arg_params
    %i(job bucket prefix)
  end

  def lua_script
    <<~LUA
      local job = ARGV[1]
      local bucket = ARGV[2]
      local prefix = ARGV[3]
      local r = redis
      r.call('lrem', prefix .. ':jobs:' .. bucket .. ':working', 1, job)
      r.call('rpush', prefix .. ':jobs:' .. bucket, job)
      return 1
    LUA
  end
end
