# frozen_string_literal: true

class Rjob::RecurringJob
  attr_reader :context
  attr_reader :cron
  attr_reader :job_class_name
  attr_reader :job_arguments
  attr_reader :unique_id

  def initialize(context, cron, job_class_name, job_arguments, unique_id=nil)
    @context = context
    @cron = cron
    @job_class_name = job_class_name
    @job_class = nil
    @job_arguments = job_arguments

    @unique_id = unique_id

    generate_unique_id! unless @unique_id
  end

  def maybe_enqueue(redis)
    key_name = "#{@context.prefix}:recurring:1:#{@unique_id}:lastrun".b
    current_time = Time.now

    last_run_str = redis.get(key_name)
    last_run = last_run_str ? Time.parse(last_run_str) : (current_time - 1)

    next_run_on = @cron.next_time(last_run)
    should_run = (current_time >= next_run_on.to_t)

    @context.enqueue_job_with_redis(job_class, job_arguments, redis) if should_run

    if should_run || last_run_str == nil
      redis.set(key_name, current_time.utc.to_s, ex: @cron.rough_frequency * 2)
    end
  end

  def job_class
    @job_class ||= @context.demodularize_class(@job_class_name)
  end

  def self.from_definition(context, defn)
    new(
      context,
      Fugit.parse(defn[:cron]),
      defn[:job_class].to_s,
      defn[:arguments],
      defn[:unique_id]
    )
  end

  private

  def generate_unique_id!
    digest = ::OpenSSL::Digest.new('sha256')
    digest << @job_class_name
    digest << @cron.original
    @job_arguments.each { |x| digest << x.to_s }

    @unique_id = digest.digest
  end
end
