# frozen_string_literal: true

module Rjob::Scripts
  SCRIPTS = {
    check_leadership: 'CheckLeadership',
    enqueue_job: 'EnqueueJob',
    schedule_job_at: 'ScheduleJobAt',
    scan_buckets: 'ScanBuckets',
    retry_job: 'RetryJob',
    return_job_execution: 'ReturnJobExecution',
    enqueue_scheduled_jobs: 'EnqueueScheduledJobs',
  }.freeze

  class ScriptRunner
    def initialize
      @scripts = {}
    end

    def load_all_scripts(redis)
      SCRIPTS.each do |file_name, class_name|
        klass = Rjob::Scripts.const_get(class_name)
        script = klass.new
        @scripts[file_name] = script
        load_script(redis, script)
      end
    end

    def exec(redis, name, *args)
      script = @scripts[name]
      redis.evalsha(script.sha1, *args)
    end

    private

    def load_script(redis, script)
      script.sha1 = redis.script(:load, script.lua_script)
    end
  end
end

require 'rjob/scripts/redis_script'

Rjob::Scripts::SCRIPTS.each do |file_name, class_name|
  require "rjob/scripts/#{file_name}"
end
