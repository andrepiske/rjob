# frozen_string_literal: true

class Rjob::Context
  attr_reader :config
  attr_reader :prefix
  attr_reader :bucket_count
  attr_reader :logger
  attr_reader :job_wrapper_proc
  attr_reader :script_runner
  attr_reader :recurring_jobs

  def self.instance
    return @instance if @instance
    raise "Rjob is not configured. Please call Rjob.configure first"
  end

  def self.set_instance(instance)
    @instance = instance
  end

  # Available options:
  #
  # :redis - (passed to Redis.new)
  # :max_threads - paralallelism
  # :bucket_count - defaults to 32
  # :redis_pool_size - redis connection pool size. Defaults to 10
  # :prefix - defaults to "rjob"
  # :job_wrapper_proc - defaults to none
  # :logger - duck-typed Logger, defaults to nil
  #
  def self.configure
    raise "Already configured!: #{@instance}" if @instance
    config = {}
    yield(config)
    set_instance(new(config))
  end

  def initialize(config)
    @config = config.dup
    @pool_size = @config.fetch(:redis_pool_size, 10)

    @bucket_count = config.fetch(:bucket_count, 32)
    @prefix = config.fetch(:prefix, 'rjob')
    @logger = config[:logger]
    @job_wrapper_proc = config[:job_wrapper_proc]
    @script_runner = Rjob::Scripts::ScriptRunner.new
    @recurring_jobs = nil

    if config.key?(:recurring_jobs)
      require "rjob/recurring"

      @recurring_jobs = config[:recurring_jobs].map do |defn|
        Rjob::RecurringJob.from_definition(self, defn)
      end
    end

    initialize_connection_pool
    load_redis_scripts
  end

  def dead_job_count()
    redis { |r| r.llen("#{@prefix}:dead") }
  end

  def get_dead_jobs(count=1, offset=0, keyname: 'ohh-dead')
    redis do |r|
      # dead_jobs = r.lrange("#{@prefix}:dead", offset, count < 0 ? -1 : (offset + count - 1))
      dead_jobs = r.lrange(keyname, offset, count < 0 ? -1 : (offset + count - 1))
      dead_jobs.map do |error_payload|
        payload = MessagePack.unpack(error_payload)

        {
          job: Rjob::Job.deserialize(self, payload['job']),
          when: Time.at(payload['when']),
          error_class: payload['error_class'],
          full_message: payload['message'],
        }
      end
    end
  end

  def redis(&block)
    @pool.with(&block)
  end

  def enqueue_job(job_class, args)
    redis(&method(:enqueue_job_with_redis).curry[job_class, args])
  end

  def enqueue_job_with_redis(job_class, args, r)
    job_data = MessagePack.pack([job_class.to_s, args])
    @script_runner.exec(r, :enqueue_job, [], [@prefix, @bucket_count, job_data])
  end

  def schedule_job_at(timestamp, job_class, args)
    job_data = MessagePack.pack([job_class.to_s, args])

    redis do |r|
      @script_runner.exec(r, :schedule_job_at, [], [timestamp.to_s, job_data, @prefix, @bucket_count])
    end
  end

  def fetch_worker_class(class_name:)
    demodularize_class(class_name)
  end

  def demodularize_class(name)
    const = Kernel
    name.split('::').each do |n|
      const = const.const_get(n)
    end
    const
  end

  def create_redis_connection
    redis_args = @config[:redis]
    Redis.new(redis_args)
  end

  private

  def load_redis_scripts
    @pool.with do |redis|
      @script_runner.load_all_scripts(redis)
    end
  end

  def initialize_connection_pool
    @pool = ConnectionPool.new(size: @pool_size) { create_redis_connection }
  end
end
