# frozen_string_literal: true

require 'redis'
require 'msgpack'

require 'connection_pool'
require 'concurrent-ruby'
require 'socket'
require 'securerandom'

require 'rjob/version'
require 'rjob/context'

require 'rjob/worker'

require 'rjob/job'
require 'rjob/job_processor'

require 'rjob/scripts'

module Rjob
  def self.configure(&block)
    ::Rjob::Context.configure(&block)
  end

  def self.enqueue(job_class, *args)
    ::Rjob::Context.instance.enqueue_job(job_class, args)
  end

  def self.schedule_in(seconds_from_now, job_class, *args)
    t = Time.now.to_i + seconds_from_now
    ::Rjob::Context.instance.schedule_job_at(t, job_class, args)
  end

  def self.schedule_at(timestamp, job_class, *args)
    ::Rjob::Context.instance.schedule_job_at(timestamp.to_i, job_class, args)
  end
end
