# frozen_string_literal: false

require "test_helper"
require "support/dummy_worker"

class BasicTest < TestCase
  def setup
    reset_context!
  end

  def test_basic_enqueuing
    ::Rjob::Context.instance.enqueue_job(DummyWorker, [1, 2, 3])

    assert_equal 1, @redis.llen("ttt:jobs:1")
    assert_equal "1", @redis.get("ttt:next")

    job = extract_job(@redis.lrange("ttt:jobs:1", 0, 1).first)
    assert_equal "1", job[0]
    assert_equal 0, job[1]

    job_data = MessagePack.unpack(job[2])
    assert_equal "DummyWorker", job_data[0]
    assert_equal [1, 2, 3], job_data[1]
  end

  def test_worker_registering
    make_worker

    assert_equal 1, @redis.llen("ttt:workers")
    assert_equal "host1-lerand", @redis.lrange("ttt:workers", 0, 1).first

    worker_data = @redis.hgetall("ttt:worker:host1-lerand")
    assert_equal({
      "heartbeat" => @time.to_s,
      "queue_length" => "0",
      "processed" => "0",
      "failed" => "0",
      "returned" => "0",
      "state" => "new"
    }, worker_data)
  end

  def test_worker_with_job_wrapper_proc
    code_passed = false

    reset_context! do |c|
      c[:job_wrapper_proc] = proc do |le_blk|
        le_blk.call

        code_passed = true
      end
    end

    make_worker

    Rjob::Context.instance.enqueue_job(DummyWorker, [1, 2, 3])

    @worker.stub :sleep, nil do
      @worker.instance_variable_set(:@state, :running)
      @worker.send(:run_iteration)
    end

    stop_and_wait_subscription_thread(@worker)

    assert code_passed
  end

  def test_worker_processing
    make_worker
    Rjob::Context.instance.enqueue_job(DummyWorker, [1, 2, 3])

    @worker.stub :sleep, nil do
      @worker.instance_variable_set(:@state, :running)
      @worker.send(:run_iteration)
    end

    working_jobs_count_snapshot = nil
    working_job_snapshot = nil
    original_new = Rjob::JobProcessor.method(:new)

    Rjob::JobProcessor.stub(:new, proc do |ctx, job_str|
      working_jobs_count_snapshot = @redis.llen("ttt:jobs:1:working")
      working_job_snapshot = @redis.lrange("ttt:jobs:1:working", 0, 1)[0]
      original_new.call(ctx, job_str)
    end) do
      stop_and_wait_subscription_thread(@worker)
    end

    assert_equal 1, working_jobs_count_snapshot

    job = extract_job(working_job_snapshot)
    assert_equal "1", job[0]
    assert_equal 0, job[1]

    job_data = MessagePack.unpack(job[2])
    assert_equal "DummyWorker", job_data[0]
    assert_equal [1, 2, 3], job_data[1]

    assert_equal 0, @redis.llen("ttt:jobs:1:working")
  end

  def test_retry_job
    make_worker
    Rjob::Context.instance.enqueue_job(DummyWorker, ["foo", 300])

    @worker.stub :sleep, nil do
      @worker.instance_variable_set(:@state, :running)
      @worker.send(:run_iteration)
    end

    stubbed_retry_options = {
      retry: true,
      max_retries: 50,
    }

    stubbed_perform = proc do
      raise DummyWorker::DummyError.new("some error")
    end

    DummyWorker.stub :retry_options, stubbed_retry_options do
      DummyWorker.stub_any_instance :perform, stubbed_perform do
        stop_and_wait_subscription_thread(@worker)
      end
    end


    assert_equal 0, @redis.llen("ttt:jobs:1:working")
    assert_equal 1, @redis.zcard("ttt:scheduled:1")

    job = extract_job(@redis.zrange("ttt:scheduled:1", 0, 1).first)
    assert_equal "1", job[0]
    assert_equal 1, job[1]

    job_data = MessagePack.unpack(job[2])
    assert_equal ["DummyWorker", ["foo", 300]], job_data
  end

  def test_too_many_retries_job
    job_data = ["DummyWorker", ["bar", 400]]
    job_str = "1!10!#{MessagePack.pack(job_data)}"
    @redis.lpush("ttt:jobs:1", job_str)

    make_worker

    stubbed_retry_options = {
      retry: true,
      max_retries: 10,
    }

    stubbed_perform = proc do
      raise DummyWorker::DummyError.new("some error")
    end

    the_time = Time.parse("2000-02-04T10:22:30Z").to_i

    Timecop.freeze(Time.at(the_time)) do
      DummyWorker.stub :retry_options, stubbed_retry_options do
        DummyWorker.stub_any_instance :perform, stubbed_perform do
          orig_sleep = Kernel.method(:sleep)

          @worker.stub :sleep, proc{ orig_sleep[0.1] } do
            @worker.instance_variable_set(:@state, :running)
            @worker.send(:run_iteration)
          end

          stop_and_wait_subscription_thread(@worker)
        end
      end
    end

    assert_equal 0, @redis.llen("ttt:jobs:1:working")
    assert_equal 0, @redis.zcard("ttt:scheduled:1")
    assert_equal 1, @redis.llen("ttt:dead")

    dead_job = MessagePack.unpack(@redis.lrange("ttt:dead", 0, 1).first)

    assert_equal Hash, dead_job.class
    assert_equal 4, dead_job.length

    assert_equal the_time, dead_job["when"]
    assert_equal "DummyWorker::DummyError", dead_job["error_class"]
    assert_equal "some error", dead_job["full_message"]
    assert_equal job_str, dead_job["job"].force_encoding(Encoding::ASCII_8BIT)
  end
end
