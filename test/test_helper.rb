require "simplecov"

SimpleCov.start

require "minitest/autorun"
require "minitest/reporters"
require "minitest/stub_any_instance"
require "timecop"

require "rjob"
require "rjob/worker_process"

Minitest::Reporters.use! [Minitest::Reporters::SpecReporter.new]

class TestCase < Minitest::Test

  # warning: use for debugging only
  def join_subscription_thread(worker)
    thread = worker.instance_variable_get(:@subscription_thread)
    raise "Thread is not alive" unless thread&.alive?

    worker.send(:disable_subscription_thread)

    thread.join
  end

  def stop_and_wait_subscription_thread(worker, timeout: 5)
    worker.send(:scan_buckets)
    sleep(0.5) # TODO: come up with a better solution :)

    thread = worker.instance_variable_get(:@subscription_thread)
    return unless thread&.alive?

    subscription_thread = worker.instance_variable_get(:@subscription_thread)
    worker.instance_variable_set(:@subscription_thread, nil)
    subscription_thread.raise(Rjob::WorkerProcess::StopSubscription.new)
    # worker.send(:disable_subscription_thread)

    time_started = Time.now.to_i

    loop do
      break unless thread.alive?

      sleep(0.1)
      if Time.now.to_i - time_started > timeout
        raise Timeout::TimeoutError.new
      end
    end

    assert_equal false, thread.status
  end

  def extract_job(job_str)
    job = Rjob::Job.deserialize(Rjob::Context.instance, job_str)

    [
      job.id,
      job.retry_num,
      job.payload
    ]
  end

  def make_worker
    @time ||= Time.now.to_i

    @worker = Socket.stub :gethostname, "host1" do
      SecureRandom.stub :alphanumeric, "lerand" do
        Rjob::WorkerProcess.new(Rjob::Context.instance)
      end
    end

    Timecop.freeze(Time.at(@time)) do
      @worker.send(:register_worker)
    end
  end

  def reset_context!
    Rjob::Context.set_instance(nil)
    Rjob::Context.configure do |c|
      c[:redis] = { url: "redis://127.0.0.1/13" }
      c[:bucket_count] = 3
      c[:prefix] = "ttt"
      if block_given?
        yield(c)
      end
    end

    @redis = Redis.new(url: "redis://127.0.0.1/13")
    @redis.flushdb

    DummyWorker.clean_executions
  end
end
