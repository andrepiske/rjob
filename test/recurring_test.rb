# frozen_string_literal: false

require "test_helper"
require "rjob/recurring"
require "support/dummy_worker"

class RecurringTest < TestCase
  RECURRING_JOB_1_KEY = "ttt:recurring:1:\x8c\xa6\xa4+\x91\x8d)\x85x\xc9q]J\xa1\xea8\xbf\x1cCpc\x85\xd8\xe0\x8b)\x169\xfcP\xc6T:lastrun".freeze

  def test_enqueuing_recurring_job
    reset_context! do |c|
      c[:recurring_jobs] = [
        { cron: "45 * * * *", job_class: "DummyWorker", arguments: [100, "qux"] } # unique_id = RECURRING_JOB_1_KEY
      ]
    end

    @time = Time.parse("2015-04-15 13:40:50 Z").to_i
    make_worker

    Timecop.freeze(Time.at(@time)) do
      @worker.send(:enqueue_recurring_jobs)
    end

    assert_equal @redis.get(RECURRING_JOB_1_KEY), "2015-04-15 13:40:50 UTC"
    assert_in_delta 7200, @redis.ttl(RECURRING_JOB_1_KEY), 5

    assert_equal 0, @redis.llen("ttt:jobs:1")
    assert_nil DummyWorker.executions

    time_in_future = Time.parse("2015-04-15 13:46:50 Z").to_i
    Timecop.freeze(Time.at(time_in_future)) do
      @worker.send(:enqueue_recurring_jobs)
    end

    assert_equal 1, @redis.llen("ttt:jobs:1")
    job = extract_job(@redis.lpop("ttt:jobs:1"))
    assert_equal ["DummyWorker", [100, "qux"]], MessagePack.unpack(job[2])
  end
end
