# frozen_string_literal: true

class DummyWorker < Rjob::Worker
  class DummyError < StandardError
  end

  def perform(*opts)
    self.class.add_execution(opts)
  end

  class << self
    attr_reader :executions

    def add_execution(value)
      @executions ||= []
      @executions << value
    end

    def clean_executions
      @executions = nil
    end
  end
end
