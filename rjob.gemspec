# frozen_string_literal: true

require File.expand_path("./lib/rjob/version", __dir__)

Gem::Specification.new do |s|
  s.name        = 'rjob'
  s.version     = Rjob::VERSION
  s.summary     = "Asynchronous job processing"
  s.description = "RJob: asynchronous job processing"
  s.author      = "André D. Piske"
  s.email       = 'andrepiske@gmail.com'
  s.homepage    = 'https://gitlab.com/andrepiske/rjob'
  s.licenses    = ['Apache-2.0']

  s.files       = Dir.glob([ "lib/**/*.rb", "bin/*" ])
  s.executables = ["rjob"]

  s.add_runtime_dependency 'oj', '< 4'
  s.add_runtime_dependency 'multi_json', '< 2'
  s.add_runtime_dependency 'redis', '>= 3.0.5'
  s.add_runtime_dependency 'msgpack', '~> 1.3'
  s.add_runtime_dependency 'connection_pool', '>= 2.2.2', '< 3'
  s.add_runtime_dependency 'concurrent-ruby', '~> 1.1.6'
end
