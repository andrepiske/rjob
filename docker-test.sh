#!/bin/sh

/bin/sh -c 'nohup redis-server &' > /dev/null

limit=10
failed=0
while [ $limit -ge 1 ]; do
  redis-cli -c info 2&>1 >/dev/null
  if [ $? -eq 0 ]; then
    sleep 0.2
    limit=$((limit-1))
  else
    limit=0
  fi
done

bundle exec rake test
